/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conservision.pointserver.common.command;

/**
 *
 * @author Venkata Rahul
 * This class implements a command that is sent to a Lattice from a POINTServer
 * It contains a nucleusId(as stored on the POINTserver, a messageId also assigned
 * on the POINTServer.
 * 
 * The POINTServer sends a latticeCommand to the Bridge TCP server, implemented
 * using Apache Mina.
 * The BridgeTCPServer implements two filters for each request. The first Filter
 * is the PANIDAssignerFilter. It translates the nucleusId of the LatticeCommand
 * to a PANId. The NucleusId-PANIdMap is stored on the file-memory of the bridge.
 * 
 * The second filter is a MessageSequenceNumberAssignerFilter. It assigns each 
 * LatticeCommand, a messageSequenceNumber.
 * Every downstream microcontroller(nucleus) can simultaneously perform only upto
 * IBridgeCostants.MAX_SERIAL_COMMANDS_PER_NUCLEUS commands. At the moment this 
 * value is being assumed to be a constant across all Nuclei even if the nucleus
 * nucleuses capabilities (hardware and software) permit more . Therefore, if the
 * bridge gets more than IBridgeCostants.MAX_SERIAL_COMMANDS_PER_NUCLEUS commands
 * to a specific nucleus, then the POINTServer has to be intimated and/or locally,
 * queued for implementation as soon as the existing queue is completed. Another 
 * purpose of the filter is to identify the specific request to which the nucleus
 * responds via the Serial Port. the messageSequenceNumber acts as an identifier
 * by mapping it to a (PanID,messageSequence) map.
 */
public class LatticeCommand extends CoreCommand{
    
    private String nucleusId;
    private String pointServerMessageId;

    public String getNucleusId() {
        return nucleusId;
    }

    public void setNucleusId(String nucleusId) {
        this.nucleusId = nucleusId;
    }

    public String getPointServerMessageId() {
        return pointServerMessageId;
    }

    public void setPointServerMessageId(String pointServerMessageId) {
        this.pointServerMessageId = pointServerMessageId;
    }
    
}