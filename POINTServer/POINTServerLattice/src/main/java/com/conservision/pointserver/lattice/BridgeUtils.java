package com.conservision.pointserver.lattice;

import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.classpath.ClasspathConfigurationSource;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;

import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Created by Venkata Rahul on 4/25/2017.
 */
public class BridgeUtils {

    private ConfigurationProvider provider;
    private static final BridgeUtils ourInstance = new BridgeUtils();

    private BridgeUtils() {
        ConfigFilesProvider fileProvider = () -> Arrays.asList(Paths.get(IBridgeConstants.PROPNAME_DEFAULTPROPFILENAME));
        if (fileProvider == null) {
            System.out.println("Could not find properties files. Bridge will exit now");
            System.exit(-1);
        }
        ConfigurationSource fileSource = new ClasspathConfigurationSource(fileProvider);
        provider = new ConfigurationProviderBuilder().withConfigurationSource(fileSource).build();
    }

    public static String getBrokerURL() {
        return ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BROKER_HOST, String.class) + ":" + ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BROKER_PORT, Integer.class);
    }

    public static String getBrokerCredentials_User() {
        return ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BROKER_CREDS_USER, String.class);
    }

    public static String getBrokerCredentials_Pswd() {
        return ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BROKER_CREDS_PSWD, String.class);
    }

    public static String getBrokerIODomain() {
        return ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BROKER_IO_DOMAIN, String.class);
    }

    public static String getBrokerDatabaseName() {
        return ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BROKER_DB_NAME, String.class);
    }

    public static String getLatticeId() {
        return ourInstance.provider.getProperty(IBridgeConstants.PROPNAME_BRIDGE_ID, String.class);
    }
}