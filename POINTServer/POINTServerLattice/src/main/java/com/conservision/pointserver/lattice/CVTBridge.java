/**
 * Created by Venkata Rahul on 4/21/2017.
 */
package com.conservision.pointserver.lattice;

public class CVTBridge {

    public CVTBridge() {
        configure();
    }

    public static void main(String[] args) {
        System.out.println("Bridge started");
        CVTBridge cvtBridge = new CVTBridge();
        cvtBridge.start();
        System.out.println("Bridge shutdown");
    }

    private void configure() {
    }

    private void start() {
        Runtime.getRuntime().addShutdownHook(new BridgeShutdownProcessor());
//        System.out.println("Press any key key to quit...");
        int i = -1;
        while(i<0){

        }
    }

    private class BridgeShutdownProcessor extends Thread{
        @Override
        public void run() {
            shutdown();
        }
    }

    private void shutdown() {
    }
}
