/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conservision.pointserver.lattice;

/**
 *
 * @author Venkata Rahul
 Thie class implements the sacred commandCode that is ultimately executed by the 
 Nucleus. It contains a commandCode, a messageSequenceNumber and a set of parameters, 
 */

public class CoreCommand {

    private int commandCode;
    private short messageSequenceNumber;
    private short numParams;
    private short[] params;

    public int getCommand() {
        return commandCode;
    }

    public void setCommand(int command) {
        this.commandCode = command;
    }

    public short getNumParams() {
        return numParams;
    }

    public void setNumParams(short numParams) {
        this.numParams = numParams;
    }

    public short[] getParams() {
        return params;
    }

    public void setParams(short[] params) {
        this.params = params;
    }
    
    public short getMessageSequenceNumber() {
        return messageSequenceNumber;
    }

    public void setMessageSequenceNumber(short messageSequenceNumber) {
        this.messageSequenceNumber = messageSequenceNumber;
    }
}