/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conservision.pointserver.lattice;

import com.conservision.pointserver.common.command.*;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;

/**
 *
 * @author Venkata Rahul
 */
public class MessageSequencerFilter extends IoFilterAdapter{
    private final String name;

    public MessageSequencerFilter() {
        this.name = "PanIDFilter";
    }
    
    @Override
    public void messageReceived(NextFilter nextFilter, IoSession session, Object message) throws Exception {
        LatticeCommand latticeCommand = (LatticeCommand)message;
        System.out.println("com.conservision.bridge.tcpImpl.PanIDFilter.messageReceived()");;
        nextFilter.messageReceived(session, message);
    }
}