/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conservision.pointserver.lattice;

import java.nio.file.Paths;
import java.util.*;
import org.cfg4j.provider.*;
import org.cfg4j.source.*;
import org.cfg4j.source.classpath.*;
import org.cfg4j.source.context.filesprovider.*;

/**
 *
 * @author Venkata Rahul
 */
public class PANIdMap {

    private ConfigurationProvider nucleusMapProvider;
    private final Map<String, Long> nucleusIdToPANIdMap;
    private final Map<Long, String> PANIdToNucleusIdMap;

    private void readNucleusPANIDMap() {
        ConfigFilesProvider nucleusMapFileProvider = () -> Arrays.asList(Paths.get(IBridgeConstants.PROPNAME_DEFAULTPROPFILENAME));
        if (nucleusMapFileProvider == null) {
            System.out.println("Could not find nucleus map file. Bridge will exit now");
            System.exit(-1);
        }
        ConfigurationSource nucleusMapSource = new ClasspathConfigurationSource(nucleusMapFileProvider);
        nucleusMapProvider = new ConfigurationProviderBuilder().withConfigurationSource(nucleusMapSource).build();
    }

    public PANIdMap() {
        this.nucleusIdToPANIdMap = new HashMap<>();
        this.PANIdToNucleusIdMap = new HashMap<>();
        readNucleusPANIDMap();
        createPANIDtoNucleusIdMap();
    }

    public void addToMap(String nucleusId, Long panId) {
        this.nucleusIdToPANIdMap.put(nucleusId, panId);
        this.persistToFile();
    }

    public void removeFromMap(String nucleusId) {
        this.nucleusIdToPANIdMap.remove(nucleusId);
        this.persistToFile();
    }

    public int getNumberOfMappedNuclei() {
        return this.nucleusIdToPANIdMap.keySet().size();
    }

    public void persistToFile() {
        Set<String> p = this.nucleusIdToPANIdMap.keySet();
    }

    public Long getPanId(String nucleusId) {
        return this.nucleusIdToPANIdMap.get(nucleusId);
    }

    public String getNucleusId(Long panId) {
        return this.PANIdToNucleusIdMap.get(panId);
    }

    private void createPANIDtoNucleusIdMap() {
        for(String key : this.nucleusIdToPANIdMap.keySet()) {
            this.PANIdToNucleusIdMap.put(this.nucleusIdToPANIdMap.get(key), key );
        }
    }
}
