/*
 * Copyright ConserVision Technologies Private Limited
 * No part of this source may be shared witb anyone, without a consent of 
 * ConserVision Technologies Limited.
 */
package com.conservision.pointserver.lattice;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
/**
 *
 * @author Venkata Rahul
 */
public class TCPRequestReceiver {

    public TCPRequestReceiver() {
        IoAcceptor acceptor = new NioSocketAcceptor();
        acceptor.getFilterChain().addLast("PanIdBinder", new PanIDAssignerFilter());
        acceptor.getFilterChain().addLast("MessageSequenceerFilter", new MessageSequencerFilter());
        try {
            acceptor.bind(new InetSocketAddress(1234));
        } catch (IOException ex) {
            Logger.getLogger(TCPRequestReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
