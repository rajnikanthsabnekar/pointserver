package com.conservision.pointserver.rest;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/")
public class PointServerRestApplication extends ResourceConfig {
    public PointServerRestApplication() {
        packages("com.conservision.pointserver.rest.svcs");
    }
}