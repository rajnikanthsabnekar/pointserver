package com.conservision.pointserver.rest.svcs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;

@Path("/")
public class PointServerRestSvcs {
    private final String USERAGENT = "Mozilla/5.0";

    private static PointServerRestSvcs abc = new PointServerRestSvcs();

    @POST
    @Path("/point/verify")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public void verifyUser(@FormParam("desiredState") String desiredState,
            @FormParam("initialState") String initialState, @FormParam("nucId") String nucId) {

        long cloudMsgId = 18446;
        long cmd = 0xdb;

        int nucIdvalue = Integer.parseInt(nucId);
        int desiredStatevalue = Integer.parseInt(desiredState);
        int initialStatevalue = Integer.parseInt(initialState);
        int size = 8;
        int complementedInitialState;

        System.out.println("initialStatevalue is " + initialStatevalue);
        System.out.println("desiredStatevalue is " + desiredStatevalue);

        String initialStatevaluebinary = String
                .format("%" + Integer.toString(size) + "s", Integer.toBinaryString(initialStatevalue))
                .replace(" ", "0");
        System.out.println(" nucIdvalue: " + nucIdvalue);
        System.out.println(" initialState Binary value: " + initialStatevaluebinary);

        String desiredStatevaluebinary = String
                .format("%" + Integer.toString(size) + "s", Integer.toBinaryString(desiredStatevalue))
                .replace(" ", "0");

        System.out.println(" desiredState Binary value: " + desiredStatevaluebinary);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < desiredStatevaluebinary.length(); i++) {
            sb.append((desiredStatevaluebinary.charAt(i) ^ initialStatevaluebinary.charAt(i)));
        }

        String result = sb.toString();
        System.out.println("XOR Value.......:" + result);

        complementedInitialState = nonLeadingZeroComplement(initialStatevalue);
        System.out.println("complementedInitialState value: " + complementedInitialState);
        String complementedInitialStatebinary = String
                .format("%" + Integer.toString(size) + "s", Integer.toBinaryString(complementedInitialState))
                .replace(" ", "0");
        System.out.println(" complementedInitialStatebinary Binary value: " + complementedInitialStatebinary);

        String[] symptomsBitArr = result.split("");
        System.out.println("symptomsBit: " + result);
        System.out.println("symptomsBitArray: " + symptomsBitArr);
        int[] symptomsArray = new int[symptomsBitArr.length];
        int[] changedIndex = new int[symptomsBitArr.length];
        int k = 0;
        for (int i = 0; i < symptomsBitArr.length; i++) {
            symptomsArray[i] = Integer.parseInt(symptomsBitArr[i]);
            if (symptomsArray[i] == 1) {
                changedIndex[k] = i;
                k++;

            }
        }
        for (int i = 0; i < k; i++) {

            System.out.println("Bulb   changed is " + changedIndex[i]);

        }
        String[] symptomsBitArr1 = initialStatevaluebinary.split("");
        System.out.println("symptomsBit: " + initialStatevaluebinary);
        System.out.println("symptomsBitArray: " + symptomsBitArr1);
        int[] symptomsArray1 = new int[symptomsBitArr1.length];

        int k1 = 0;
        for (int i = 0; i < symptomsBitArr1.length; i++) {
            symptomsArray1[i] = Integer.parseInt(symptomsBitArr1[i]);
            System.out.println("index is  " + i + "value is  " + symptomsArray1[i]);
        }
        int[] changedstate = new int[symptomsBitArr1.length];

        for (int i = 0; i < k; i++) {
            System.out.println("index is  " + i + "changedIndex[i]  " + changedIndex[i]);
            System.out
                    .println("index is  " + i + "symptomsArray1[changedIndex[i]]  " + symptomsArray1[changedIndex[i]]);
            changedstate[i] = 1 - symptomsArray1[changedIndex[i]];

        }
        for (int i = 0; i < k; i++) {
            System.out.println("index is  " + i + "value is  " + changedstate[i]);
        }
        for (int i = 0; i < k; i++) {
            abc.sendCmdToLattice(cloudMsgId, nucIdvalue, cmd, changedIndex[i], changedstate[i]);
            try {
                System.out.println("Waiting...starts");
                Thread.sleep(7 * 1000);
                System.out.println("Waiting...closed");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static int nonLeadingZeroComplement(int n) {
        if (n == 0) {
            return ~n;
        }
        if (n == 1) {
            return 0;
        }

        // This line is to find how much the non-leading zero (NLZ) bits count.
        // This operation is same like: ceil(log2(n))
        int binaryBitsCount = Integer.SIZE - Integer.numberOfLeadingZeros(n - 1);

        // We use the NLZ bits count to generate sequence of 1 bits as much as
        // the NLZ bits count as complementer
        // by using shift left trick that equivalent to: 2 raised to power of
        // binaryBitsCount.
        // 1L is one value with Long literal that used here because there is
        // possibility binaryBitsCount is 32
        // (if the input is -1 for example), thus it will produce 2^32 result
        // whom value can't be contained in
        // java signed int type.
        int oneBitsSequence = (int) ((1L << binaryBitsCount) - 1);

        // XORing the input value with the sequence of 1 bits
        return n ^ oneBitsSequence;
    }

    @POST
    @Path("/point/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces(MediaType.APPLICATION_JSON)
    public void updateMessageStatus(@FormParam("messageId") String messageId, @FormParam("intvalue") String intvalue) {

        System.out.println(" messageId value: " + messageId);
        int intval = Integer.parseInt(intvalue);
        System.out.println(" intvalue value: " + intval);

    }

    private int sendCmdToLattice(long cloudMsgId, int nucId, long cmd, int device, int status) {

        try {

            URL latticURL = new URL(
                    "http://192.168.0.111:24367?psMsgId=1844&nucId=1&cmd=d7&np=2&p_0=" + device + "&p_1=" + status);
            System.out.println("URL is  " + latticURL);
            HttpURLConnection latticeConnection = (HttpURLConnection) latticURL.openConnection();

            latticeConnection.setRequestMethod("GET");

            latticeConnection.setRequestProperty("User-Agent", USERAGENT);

            int responseCode = latticeConnection.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + latticURL);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(latticeConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return 1;
    }
}
