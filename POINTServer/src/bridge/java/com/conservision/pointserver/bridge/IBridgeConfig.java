package com.conservision.pointserver.bridge;

/**
 * Created by Venkata Rahul on 4/25/2017.
 */
public interface IBridgeConfig {
    String brokerURLHost();
    String brokerURLPort();
    String brokerUserName();
    String brokerPassword();
}
