package com.conservision.pointserver.bridge;

/**
 * Created by Venkata Rahul on 4/24/2017.
 */
public class IBridgeConstants {
    public static final String PROPNAME_DEFAULTPROPFILENAME = "bridge.config";
    public static final String PROPNAME_BRIDGE_ID = "BRIDGE_ID";

    public static final String PROPNAME_BROKER_HOST = "BROKER_HOST";
    public static final String PROPNAME_BROKER_PORT = "BROKER_PORT";

    public static final String PROPNAME_BROKER_IO_DOMAIN="IO_DOMAIN";
    public static final String PROPNAME_BROKER_CREDS_USER = "BROKER_CREDS_USER";
    //Replace this with the LatticeId
    public static final String PROPNAME_BROKER_CREDS_PSWD = "BROKER_CREDS_PSWD";
    //Replace this with the LatticeId
    public static final String PROPNAME_POINT_SERVER_URL = "POINT_SERVER_URL";
    public static final String PROPNAME_POINT_SERVER_PORT = "POINT_SERVER_PORT";
    public static final String PROPNAME_BROKER_IO_STUFF = "BROKER_STUFF";//Replace this with the LatticeId

    public static final String PROP_COORD_STAYALIVE_PILL_FILENAME = "STAYALIVE_PILL_FILENAME";
    public static final String PROP_COORD_STAYALIVE_CHECK_INTERVAL = "STAYALIVE_CHECK_INTERVAL";

    public static final String PROPNAME_BROKER_DB_NAME = "BROKER_DB_NAME";

    public static final String MQCONFIG_REQ_TOPIC_RECEIVER_SUBS = "receive";
    public static final String MQCONFIG_REQ_TOPIC_RECEIVER_PUBL = "for_delegation_to_154";
    public static final String MQCONFIG_REQ_TOPIC_154DELEGATOR_SUBS = MQCONFIG_REQ_TOPIC_RECEIVER_PUBL;
    public static final String MQCONFIG_RES_TOPIC_154DELEGATOR_SUBS = "";

    public static final String BRIDGE_SHUTDOWN_CHARACTER = "q";
    public static final String RECEIVER_SUBSCRIBE_THING = "transmit";
}