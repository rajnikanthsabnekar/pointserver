/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conservision.pointserver.bridge;

import com.conservision.pointserver.common.command.*;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;

/**
 *
 * @author Venkata Rahul
 */
public class PanIDAssignerFilter extends IoFilterAdapter{
    private final String name;
    private final PANIdMap panIdMap;
    public PanIDAssignerFilter() {
        this.name = "PanIDFilter";
        this.panIdMap = new PANIdMap();
    }
    
    @Override
    public void messageReceived(NextFilter nextFilter, IoSession session, Object message) throws Exception {
        LatticeCommand latticeCommand = (LatticeCommand)message;
        Long panId = this.panIdMap.getPanId(latticeCommand.getNucleusId());
        System.out.println("Message received for Nucleus Id: " + latticeCommand.getNucleusId() + " assigned PAN ID: " + panId);
        System.out.println("com.conservision.bridge.tcpImpl.PanIDFilter.messageReceived()");;
        nextFilter.messageReceived(session, message);
    }
}